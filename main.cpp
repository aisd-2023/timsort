﻿#include <iostream>
#include <time.h>
#include <chrono>
#include "timsort.h"

const int SMALL_ARRAY_SIZE = 10;
const int BIG_ARRAY_SIZE = 1000000;
const int ORDERED_PART_SIZE = 32;

void print(int* vec, int size)
{
	for (int i = 0; i < size; i++) {
		std::cout << vec[i] << " ";
	}
	std::cout << std::endl;
}

bool testSort(int* vec, int size)
{
	auto startTime = std::chrono::system_clock::now();
	try {
		TimSort::sort(&vec[0], &vec[size]);
	}
	catch (...) {
		std::cerr << "Exception while sorting" << std::endl;
		return false;
	}
	auto endTime = std::chrono::system_clock::now();
	auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
	long duration = millis.count();
	std::cout << "Sorting time: " << duration << " ms" << std::endl;
	std::cout << std::endl;

	return true;
}

bool test1()
{
	std::cout << "Test 1: Sort small random array" << std::endl;
	const int size = SMALL_ARRAY_SIZE;
	srand(time(nullptr));

	int* vec = new int[size];
	for (int i = 0; i < size; ++i) {
		int num = rand() % size;
		vec[i] = num;
	}

	std::cout << "Array before sorting:" << std::endl;
	print(vec, size);

	try {
		TimSort::sort(&vec[0], &vec[size]);
	}
	catch (...) {
		std::cerr << "Exception while sorting" << std::endl;
		return false;
	}

	std::cout << "Array after sorting:" << std::endl;
	print(vec, size);
	std::cout << std::endl;

	delete[] vec;

	return true;
}

bool test2()
{
	std::cout << "Test 2: Sort big ordered array" << std::endl;
	const int size = BIG_ARRAY_SIZE;
	std::cout << "Array size: " << size << std::endl;

	int* vec = new int[size];
	for (int i = 0; i < size; ++i) {
		int num = i * 2;
		vec[i] = num;
	}

	bool res = testSort(vec, size);

	delete[] vec;

	return res;
}

bool test3()
{
	std::cout << "Test 3: Sort big inverted ordered array" << std::endl;
	const int size = BIG_ARRAY_SIZE;
	std::cout << "Array size: " << size << std::endl;

	int* vec = new int[size];
	for (int i = 0; i < size; ++i) {
		int num = i * (-1);
		vec[i] = num;
	}

	bool res = testSort(vec, size);

	delete[] vec;

	return res;
}

bool test4()
{
	std::cout << "Test 4: Sort big alternating array" << std::endl;
	const int size = BIG_ARRAY_SIZE;
	std::cout << "Array size: " << size << std::endl;

	int* vec = new int[size];
	for (int i = 0; i < size; ++i) {
		int num = i * (i % 2 ? 1 : -1);
		vec[i] = num;
	}

	bool res = testSort(vec, size);

	delete[] vec;

	return res;
}

bool test5()
{
	std::cout << "Test 5: Sort big partly ordered array" << std::endl;
	const int size = BIG_ARRAY_SIZE;
	std::cout << "Array size: " << size << std::endl;

	int* vec = new int[size];
	int val = 0;
	for (int i = 0; i < size; ++i) {
		val++;
		if (i % ORDERED_PART_SIZE == 0) val -= ORDERED_PART_SIZE / 2;
		vec[i] = val;
	}

	bool res = testSort(vec, size);

	delete[] vec;

	return res;
}

bool test6()
{
	std::cout << "Test 6: Sort big random array" << std::endl;
	const int size = BIG_ARRAY_SIZE;
	std::cout << "Array size: " << size << std::endl;
	srand(time(nullptr));

	int* vec = new int[size];
	for (int i = 0; i < size; ++i) {
		int num = rand() % size;
		vec[i] = num;
	}

	bool res = testSort(vec, size);

	delete[] vec;

	return res;
}

int main()
{
	if (!test1()) return -1;
	if (!test2()) return -1;
	if (!test3()) return -1;
	if (!test4()) return -1;
	if (!test5()) return -1;
	if (!test6()) return -1;

	return 0;
}

