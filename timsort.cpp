#include <algorithm>
#include "timsort.h"

TimSort::TimSort()
	: minGallop_(MIN_GALLOP) 
{
}

TimSort::~TimSort() 
{
}

void TimSort::binarySort(int* lo, int* hi, int* start) {
	if (start == lo) {
		++start;
	}
	for (; start < hi; ++start) {
		int pivot = *start;

		int* pos = std::upper_bound(lo, start, pivot);
		for (int* p = start; p > pos; --p) {
			*p = *(p - 1);
		}
		*pos = pivot;
	}
}

int TimSort::countRunAndMakeAscending(int* lo, int* hi) {

	int* runHi = lo + 1;
	if (runHi == hi) {
		return 1;
	}

	if (*runHi < *lo) {
		do {
			++runHi;
		} while (runHi < hi && (*runHi < *(runHi - 1)));
		std::reverse(lo, runHi);
	}
	else {
		do {
			++runHi;
		} while (runHi < hi && !(*runHi < *(runHi - 1)));
	}

	return runHi - lo;
}

int TimSort::minRunLength(int n) {

	int r = 0;
	while (n >= 2 * MIN_MERGE) {
		r |= (n & 1);
		n >>= 1;
	}
	return n + r;
}

void TimSort::pushRun(int* runBase, int runLen) {
	pending_.emplace_back(run(runBase, runLen));
}

void TimSort::mergeCollapse() {
	while (pending_.size() > 1) {
		int n = pending_.size() - 2;

		if ((n > 0 && pending_[n - 1].len <= pending_[n].len + pending_[n + 1].len) ||
			(n > 1 && pending_[n - 2].len <= pending_[n - 1].len + pending_[n].len)) {
			if (pending_[n - 1].len < pending_[n + 1].len) {
				--n;
			}
			mergeAt(n);
		}
		else if (pending_[n].len <= pending_[n + 1].len) {
			mergeAt(n);
		}
		else break;
	}
}

void TimSort::mergeForceCollapse() {
	while (pending_.size() > 1) {
		int n = pending_.size() - 2;

		if (n > 0 && pending_[n - 1].len < pending_[n + 1].len) {
			n--;
		}
		mergeAt(n);
	}
}

void TimSort::mergeAt(int i) {
	int stackSize = pending_.size();

	int* base1 = pending_[i].base;
	int len1 = pending_[i].len;
	int* base2 = pending_[i + 1].base;
	int len2 = pending_[i + 1].len;

	pending_[i].len = len1 + len2;

	if (i == stackSize - 3) {
		pending_[i + 1] = pending_[i + 2];
	}

	pending_.pop_back();

	mergeConsecutiveRuns(base1, len1, base2, len2);
}

void TimSort::mergeConsecutiveRuns(int* base1, int len1, int* base2, int len2) {

	int k = gallopRight(*base2, base1, len1, 0);

	base1 += k;
	len1 -= k;

	if (len1 == 0) {
		return;
	}

	len2 = gallopLeft(*(base1 + (len1 - 1)), base2, len2, len2 - 1);
	if (len2 == 0) {
		return;
	}

	if (len1 <= len2) {
		mergeLo(base1, len1, base2, len2);
	}
	else {
		mergeHi(base1, len1, base2, len2);
	}
}

int TimSort::gallopLeft(int key, int* base, int len, int hint) {
	int lastOfs = 0;
	int ofs = 1;

	if ((*(base + hint) < key)) {
		int maxOfs = len - hint;
		while (ofs < maxOfs && (*(base + (hint + ofs)) < key)) {
			lastOfs = ofs;
			ofs = (ofs << 1) + 1;

			if (ofs <= 0) {
				ofs = maxOfs;
			}
		}
		if (ofs > maxOfs) {
			ofs = maxOfs;
		}

		lastOfs += hint;
		ofs += hint;
	}
	else {
		int maxOfs = hint + 1;
		while (ofs < maxOfs && !(*(base + (hint - ofs)) < key)) {
			lastOfs = ofs;
			ofs = (ofs << 1) + 1;

			if (ofs <= 0) {
				ofs = maxOfs;
			}
		}
		if (ofs > maxOfs) {
			ofs = maxOfs;
		}

		int tmp = lastOfs;
		lastOfs = hint - ofs;
		ofs = hint - tmp;
	}

	return std::lower_bound(base + (lastOfs + 1), base + ofs, key) - base;
}

int TimSort::gallopRight(int key, int* base, int len, int hint) {

	int ofs = 1;
	int lastOfs = 0;

	if (key < *(base + hint)) {
		int maxOfs = hint + 1;
		while (ofs < maxOfs && (key < *(base + (hint - ofs)))) {
			lastOfs = ofs;
			ofs = (ofs << 1) + 1;

			if (ofs <= 0) {
				ofs = maxOfs;
			}
		}
		if (ofs > maxOfs) {
			ofs = maxOfs;
		}

		int tmp = lastOfs;
		lastOfs = hint - ofs;
		ofs = hint - tmp;
	}
	else {
		int maxOfs = len - hint;
		while (ofs < maxOfs && !(key < *(base + (hint + ofs)))) {
			lastOfs = ofs;
			ofs = (ofs << 1) + 1;

			if (ofs <= 0) {
				ofs = maxOfs;
			}
		}
		if (ofs > maxOfs) {
			ofs = maxOfs;
		}

		lastOfs += hint;
		ofs += hint;
	}

	return std::upper_bound(base + (lastOfs + 1), base + ofs, key) - base;
}

void TimSort::rotateLeft(int* first, int* last) {
	int tmp = *first;
	auto last_1 = std::move(first + 1, last, first);
	*last_1 = tmp;
}

void TimSort::rotateRight(int* first, int* last) {
	auto last_1 = last - 1;
	int tmp = *last_1;
	std::move_backward(first, last_1, last);
	*first = tmp;
}


void TimSort::mergeLo(int* base1, int len1, int* base2, int len2) {

	if (len1 == 1) {
		return rotateLeft(base1, base2 + len2);
	}
	if (len2 == 1) {
		return rotateRight(base1, base2 + len2);
	}

	copy_to_tmp(base1, len1);

	int* cursor1 = tmp_;
	int* cursor2 = base2;
	int* dest = base1;

	*dest = *cursor2;
	++cursor2;
	++dest;
	--len2;

	int minGallop(minGallop_);

	bool isToBreak = false;

	while (true) {
		int count1 = 0;
		int count2 = 0;

		do {
			if (*cursor2 < *cursor1) {
				*dest = *cursor2;
				++cursor2;
				++dest;
				++count2;
				count1 = 0;
				if (--len2 == 0) {
					isToBreak = true;
					break;
				}
			}
			else {
				*dest = *cursor1;
				++cursor1;
				++dest;
				++count1;
				count2 = 0;
				if (--len1 == 1) {
					isToBreak = true;
					break;
				}
			}
		} while ((count1 | count2) < minGallop);
		if (isToBreak) break;

		do {
			count1 = gallopRight(*cursor2, cursor1, len1, 0);
			if (count1 != 0) {
				std::move_backward(cursor1, cursor1 + count1, dest + count1);
				dest += count1;
				cursor1 += count1;
				len1 -= count1;

				if (len1 <= 1) {
					isToBreak = true;
					break;
				}
			}
			*dest = *cursor2;
			++cursor2;
			++dest;
			if (--len2 == 0) {
				isToBreak = true;
				break;
			}

			count2 = gallopLeft(*cursor1, cursor2, len2, 0);
			if (count2 != 0) {
				std::move(cursor2, cursor2 + count2, dest);
				dest += count2;
				cursor2 += count2;
				len2 -= count2;
				if (len2 == 0) {
					isToBreak = true;
					break;
				}
			}
			*dest = *cursor1;
			++cursor1;
			++dest;
			if (--len1 == 1) {
				isToBreak = true;
				break;
			}

			--minGallop;
		} while ((count1 >= MIN_GALLOP) | (count2 >= MIN_GALLOP));
		if (isToBreak) break;

		if (minGallop < 0) {
			minGallop = 0;
		}
		minGallop += 2;
	}

	minGallop_ = std::min(minGallop, 1);

	if (len1 == 1) {
		std::move(cursor2, cursor2 + len2, dest);
		*(dest + len2) = *cursor1;
	}
	else {
		std::move(cursor1, cursor1 + len1, dest);
	}
}

void TimSort::mergeHi(int* base1, int len1, int* base2, int len2) {
	if (len1 == 1) {
		return rotateLeft(base1, base2 + len2);
	}
	if (len2 == 1) {
		return rotateRight(base1, base2 + len2);
	}

	copy_to_tmp(base2, len2);

	int* cursor1 = base1 + len1;
	int* cursor2 = tmp_ + (len2 - 1);
	int* dest = base2 + (len2 - 1);

	*dest = *(--cursor1);
	--dest;
	--len1;

	int minGallop(minGallop_);

	bool isToBreak = false;

	while (true) {
		int count1 = 0;
		int count2 = 0;

		--cursor1;

		do {
			if (*cursor2 < *cursor1) {
				*dest = *cursor1;
				--dest;
				++count1;
				count2 = 0;
				if (--len1 == 0) {
					isToBreak = true;
					break;
				}
				--cursor1;
			}
			else {
				*dest = *cursor2;
				--cursor2;
				--dest;
				++count2;
				count1 = 0;
				if (--len2 == 1) {
					++cursor1;
					isToBreak = true;
					break;
				}
			}
		} while ((count1 | count2) < minGallop);
		if (isToBreak) break;
		++cursor1;

		do {
			count1 = len1 - gallopRight(*cursor2, base1, len1, len1 - 1);
			if (count1 != 0) {
				dest -= count1;
				cursor1 -= count1;
				len1 -= count1;
				std::move_backward(cursor1, cursor1 + count1, dest + (1 + count1));

				if (len1 == 0) {
					isToBreak = true;
					break;
				}
			}
			*dest = *cursor2;
			--cursor2;
			--dest;
			if (--len2 == 1) {
				isToBreak = true;
				break;
			}

			count2 = len2 - gallopLeft(*(cursor1 - 1), tmp_, len2, len2 - 1);
			if (count2 != 0) {
				dest -= count2;
				cursor2 -= count2;
				len2 -= count2;
				std::move(cursor2 + 1, cursor2 + (1 + count2), dest + 1);
				if (len2 <= 1) {
					isToBreak = true;
					break;
				}
			}
			*dest = *(--cursor1);
			--dest;
			if (--len1 == 0) {
				isToBreak = true;
				break;
			}

			--minGallop;
		} while ((count1 >= MIN_GALLOP) | (count2 >= MIN_GALLOP));
		if (isToBreak) break;

		if (minGallop < 0) {
			minGallop = 0;
		}
		minGallop += 2;
	}

	minGallop_ = std::min(minGallop, 1);

	if (len2 == 1) {
		dest -= len1;
		std::move_backward(cursor1 - len1, cursor1, dest + (1 + len1));
		*dest = *cursor2;
	}
	else {
		std::move(tmp_, tmp_ + len2, dest - (len2 - 1));
	}
}

void TimSort::copy_to_tmp(int* begin, int len) {
	if (tmp_ != nullptr) delete tmp_;
	tmp_ = new int[len];
	memcpy(tmp_, begin, len * sizeof(int));
}

void TimSort::sort(int* lo, int* hi) {
	int nRemaining = (hi - lo);
	if (nRemaining < 2) {
		return;
	}

	if (nRemaining < MIN_MERGE) {
		int initRunLen = countRunAndMakeAscending(lo, hi);
		binarySort(lo, hi, lo + initRunLen);
		return;
	}

	TimSort ts;
	int minRun = minRunLength(nRemaining);
	int* cur = lo;
	do {
		int runLen = countRunAndMakeAscending(cur, hi);

		if (runLen < minRun) {
			int force = std::min(nRemaining, minRun);
			binarySort(cur, cur + force, cur + runLen);
			runLen = force;
		}

		ts.pushRun(cur, runLen);
		ts.mergeCollapse();

		cur += runLen;
		nRemaining -= runLen;
	} while (nRemaining != 0);

	ts.mergeForceCollapse();
}
