#pragma once

#include <vector>

class TimSort {

	static constexpr int MIN_MERGE = 32;
	static constexpr int MIN_GALLOP = 7;

	struct run {
		int* base;
		int len;
		run(int* b, int l) : base(b), len(l) {}
	};

	int minGallop_;
	int* tmp_;
	std::vector<run> pending_;

	TimSort();
	~TimSort();

	static void binarySort(int* lo, int* hi, int* start);
	static int countRunAndMakeAscending(int* lo, int* hi);
	static int minRunLength(int n);

	void pushRun(int* runBase, int runLen);

	void mergeCollapse();
	void mergeForceCollapse();
	void mergeAt(int i);
	void mergeConsecutiveRuns(int* base1, int len1, int* base2, int len2);

	static int gallopLeft(int key, int* base, int len, int hint);
	static int gallopRight(int key, int* base, int len, int hint);
	static void rotateLeft(int* first, int* last);
	static void rotateRight(int* first, int* last);

	void mergeLo(int* base1, int len1, int* base2, int len2);
	void mergeHi(int* base1, int len1, int* base2, int len2);
	void copy_to_tmp(int* begin, int len);

public:
	static void sort(int* lo, int* hi);
};
